﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class MeshCreator : MonoBehaviour
{
    private CoordinateAxisController coordinateAxis = new CoordinateAxisController();

    public void BakeByMaterials()
    {
        Material[] materials = GetChildMaterials();
        List<Material[]> splittedByShaderMaterials = SplitMaterialsByShader(materials);

        MeshFilter[] filters = GetChildMeshFilters();
        List<MeshFilter[]> splittedByGroupMeshFilters =
            SplitMeshFiltersByMaterialGroups(splittedByShaderMaterials, filters);

        List<MeshRenderer[]> splittedByGroupMeshRenderers =
            SplitMeshRenderersByMaterialGroups(splittedByGroupMeshFilters);

        List<BakeStruct> bakes = new List<BakeStruct>();
        for (int i = 0; i < splittedByShaderMaterials.Count; i++)
        {
            BakeStruct bake = new BakeStruct();
            bake.materials = splittedByShaderMaterials[i];
            bake.filters = splittedByGroupMeshFilters[i];
            bake.renderers = splittedByGroupMeshRenderers[i];
            bakes.Add(bake);
        }

        HideChilds();

        for (int i = 0; i < bakes.Count; i++)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(transform);
            go.transform.localPosition = Vector3.zero;
            go.AddComponent<MeshFilter>();
            go.AddComponent<MeshRenderer>();
            StartBaking(bakes[i], go.transform);
        }
    }

    private List<MeshRenderer[]> SplitMeshRenderersByMaterialGroups(List<MeshFilter[]> splittedByGroupMeshFilters)
    {
        List<MeshRenderer>[] splittedByGroupMeshRenderers = new List<MeshRenderer>[splittedByGroupMeshFilters.Count];
        for (int i = 0; i < splittedByGroupMeshFilters.Count; i++)
        {
            splittedByGroupMeshRenderers[i] = new List<MeshRenderer>();
            foreach (var filter in splittedByGroupMeshFilters[i])
            {
                splittedByGroupMeshRenderers[i].Add(filter.GetComponent<MeshRenderer>());
            }
        }

        List<MeshRenderer[]> result = new List<MeshRenderer[]>(splittedByGroupMeshRenderers.Length);
        for(int i = 0; i < splittedByGroupMeshRenderers.Length; i++)
        {
            result.Add(splittedByGroupMeshRenderers[i].ToArray());
        }

        return result;
    }

    public void StartBaking()
    {
        MeshFilter[] filters = GetChildMeshFilters();
        Material[] materials = GetChildMaterials();

        Rect[] texturePositions;
        int[] materialTexIndexes;

        List<Material[]> splittedByShaderMaterials = SplitMaterialsByShader(materials);

        Texture2D atlas = TexturesCreator.CreateTexture(materials, out texturePositions, out materialTexIndexes);
        Mesh finalMesh = CombineMeshes(filters, materials, texturePositions, materialTexIndexes);

        transform.GetComponent<MeshFilter>().sharedMesh = finalMesh;
        Material material = new Material(Shader.Find("Standard"));
        material.SetTexture("_MainTex", atlas);
        material.SetColor(0, materials[0].color);
        transform.GetComponent<MeshRenderer>().sharedMaterial = material;

        HideChilds();
    }

    public void StartBaking(BakeStruct bake, Transform bakeTransform)
    {
        Rect[] texturePositions;
        int[] materialTexIndexes;

        List<string> textureNames = new List<string>();
        textureNames.Add("_BumpMap");
        
        List<Texture2D> atlases = new List<Texture2D>();
        for (int i = 0; i < textureNames.Count; i++)
        {
            atlases.Add(TexturesCreator.CreateTexture(bake.materials, out texturePositions, out materialTexIndexes, textureNames[i]));
        }

        Texture2D atlas = TexturesCreator.CreateTexture(bake.materials, out texturePositions, out materialTexIndexes);
        Mesh finalMesh = CombineMeshes(bake.filters, bake.materials, bake.renderers, texturePositions, materialTexIndexes);

        bakeTransform.GetComponent<MeshFilter>().sharedMesh = finalMesh;
        Material material = new Material(bake.materials[0]);
        material.CopyPropertiesFromMaterial(bake.materials[0]);
        material.SetTexture("_MainTex", atlas);
        for (int i = 0; i < atlases.Count; i++)
        {
            material.SetTexture(textureNames[i], atlases[i]);
        }
        
        bakeTransform.GetComponent<MeshRenderer>().sharedMaterial = material;
    }

    private Mesh CombineMeshes(MeshFilter[] filters, Material[] materials, MeshRenderer[] renderers, Rect[] texPositions, int[] materialIndexes)
    {
        coordinateAxis.PrepareTransformCoordinates(transform);

        Mesh finalMesh = new Mesh();
        List<CombineInstance> combines = new List<CombineInstance>();
        List<Mesh> submeshes = new List<Mesh>();
        Dictionary<MeshFilter, Vector2[]> originalUVDict = new Dictionary<MeshFilter, Vector2[]>();

        for (int i = 0; i < materials.Length; i++)
        {
            List<CombineInstance> submeshCombines = new List<CombineInstance>();
            for (int j = 0; j < filters.Length; j++)
            {
                List<Material> localMaterials = renderers[j].sharedMaterials.ToList();
                for (int k = 0; k < localMaterials.Count; k++)
                {
                    if (localMaterials[k] == materials[i])
                    {
                        int maintexRectIndex = materialIndexes.ToList().IndexOf(i);
                        Mesh mesh = filters[j].sharedMesh;
                        if (!originalUVDict.ContainsKey(filters[j]))
                        {
                            originalUVDict.Add(filters[j], mesh.uv);
                        }

                        CombineInstance ci = new CombineInstance();
                        ci.mesh = filters[j].sharedMesh;
                        Vector2[] uv = ConvertUVCoordinates(mesh.uv, texPositions[maintexRectIndex]);
                        ci.subMeshIndex = k;
                        ci.mesh.uv = uv;
                        ci.transform = filters[j].transform.localToWorldMatrix;
                        submeshCombines.Add(ci);
                    }
                }
            }

            Mesh submesh = new Mesh();
            submesh.CombineMeshes(submeshCombines.ToArray(), true);
            submeshes.Add(submesh);
        }

        foreach (var submesh in submeshes)
        {
            CombineInstance ci = new CombineInstance();
            ci.mesh = submesh;
            ci.subMeshIndex = 0;
            ci.transform = Matrix4x4.identity;
            combines.Add(ci);
        }

        coordinateAxis.ReturnTransformCoordinatesToOriginal(transform);

        finalMesh.CombineMeshes(combines.ToArray());

        for (int i = 0; i < filters.Length; i++)
        {
            filters[i].sharedMesh.uv = originalUVDict[filters[i]];
        }
        return finalMesh;
    }

    private Mesh CombineMeshes(MeshFilter[] filters, Material[] materials, Rect [] texPositions, int [] materialIndexes)
    {
        coordinateAxis.PrepareTransformCoordinates(transform);

        Mesh finalMesh = new Mesh();
        List<CombineInstance> combines = new List<CombineInstance>();
        List<Mesh> submeshes = new List<Mesh>();
        Dictionary<MeshFilter, Vector2[]> originalUVDict = new Dictionary<MeshFilter, Vector2[]>();

        for (int i = 0; i < materials.Length; i++)
        {
            List<CombineInstance> submeshCombines = new List<CombineInstance>();
            for (int j = 0; j < filters.Length; j++)
            {
                List<Material> localMaterials = filters[j].GetComponent<MeshRenderer>().sharedMaterials.ToList();
                for (int k = 0; k < localMaterials.Count; k++)
                {
                    if (localMaterials[k] == materials[i])
                    {
                        int maintexRectIndex = materialIndexes.ToList().IndexOf(i);
                        Mesh mesh = filters[j].sharedMesh;
                        if (!originalUVDict.ContainsKey(filters[j]))
                        {
                            originalUVDict.Add(filters[j], mesh.uv);
                        }
                        
                        CombineInstance ci = new CombineInstance();
                        ci.mesh = filters[j].sharedMesh;
                        Vector2[] uv = ConvertUVCoordinates(mesh.uv, texPositions[maintexRectIndex]);
                        ci.subMeshIndex = k;
                        ci.mesh.uv = uv;
                        ci.transform = filters[j].transform.localToWorldMatrix;
                        submeshCombines.Add(ci);
                    }
                }
            }

            Mesh submesh = new Mesh();
            submesh.CombineMeshes(submeshCombines.ToArray(), true);
            submeshes.Add(submesh);
        }

        foreach (var submesh in submeshes)
        {
            CombineInstance ci = new CombineInstance();
            ci.mesh = submesh;
            ci.subMeshIndex = 0;
            ci.transform = Matrix4x4.identity;
            combines.Add(ci);
        }

        coordinateAxis.ReturnTransformCoordinatesToOriginal(transform);

        finalMesh.CombineMeshes(combines.ToArray());

        for (int i = 0; i < filters.Length; i++)
        {
            filters[i].sharedMesh.uv = originalUVDict[filters[i]];
        }
        return finalMesh;
    }

    private void HideChilds()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private Vector2[] ConvertUVCoordinates(Vector2[] sourceUVs, Rect newTexturePosition)
    {
        Vector2[] convertUvCoordinates = new Vector2[sourceUVs.Length];
        for (int i = 0; i < sourceUVs.Length; i++)
        {
            convertUvCoordinates[i] = new Vector2(sourceUVs[i].x * newTexturePosition.width + newTexturePosition.x, sourceUVs[i].y * newTexturePosition.height + newTexturePosition.y);
        }
        return convertUvCoordinates;
    }
    
    private MeshFilter[] GetChildMeshFilters()
    {
        List<MeshFilter> filters = GetComponentsInChildren<MeshFilter>().ToList();
        for (int i = 0; i < filters.Count; i++)
        {
            if (filters[i].transform == transform)
            {
                filters.RemoveAt(i);
                break;
            }
        }

        return filters.ToArray();
    }

    private Material[] GetChildMaterials()
    {
        List<MeshRenderer> renderers = GetComponentsInChildren<MeshRenderer>().ToList();
        List<Material> materials = new List<Material>();
        for (int i = 0; i < renderers.Count; i++)
        {
            if (renderers[i].transform == transform)
            {
                continue;
            }

            Material[] meshMaterials = renderers[i].sharedMaterials;
            foreach (var material in meshMaterials)
            {
                if (!materials.Contains(material))
                {
                    materials.Add(material);
                }
            }
        }
        return materials.ToArray();
    }

    private List<Material[]> SplitMaterialsByShader(Material[] materials)
    {
        List<string> shaderNamesList = new List<string>();
        List<List<Material>> materialsGroupsList = new List<List<Material>>();
        foreach (var material in materials)
        {
            string shaderName = material.shader.name;
            if (!shaderNamesList.Contains(shaderName))
            {
                shaderNamesList.Add(shaderName);
                materialsGroupsList.Add(new List<Material>());
            }

            int groupIndex = shaderNamesList.FindIndex(n => n.Equals(shaderName));
            materialsGroupsList[groupIndex].Add(material);
        }
        List<Material[]> result = new List<Material[]>();
        foreach (var group in materialsGroupsList)
        {
            result.Add(group.ToArray());
        }

        return result;
    }

    private List<MeshFilter[]> SplitMeshFiltersByMaterialGroups(List<Material[]> splittedByShaderMaterials, MeshFilter[] filters)
    {
        List<MeshFilter> [] splittedByShaderMeshFilters = new List<MeshFilter>[splittedByShaderMaterials.Count];
        for (int i = 0; i < filters.Length; i++)
        {
            bool matchFilter = false;
            MeshRenderer renderer = filters[i].transform.GetComponent<MeshRenderer>();
            if (renderer != null)
            {
                Material material = renderer.sharedMaterial;
                if (material != null)
                {
                    for (int j = 0; j < splittedByShaderMaterials.Count; j++)
                    {
                        Material[] splittedMaterialsGroup = splittedByShaderMaterials[j];
                        for (int k = 0; k < splittedMaterialsGroup.Length; k++)
                        {
                            if (splittedMaterialsGroup[k] == material)
                            {
                                if (splittedByShaderMeshFilters[j] == null)
                                {
                                    splittedByShaderMeshFilters[j] = new List<MeshFilter>();
                                }

                                splittedByShaderMeshFilters[j].Add(filters[i]);
                                matchFilter = true;
                                break;
                            }
                        }
                        if (matchFilter)
                        {
                            break;
                        }
                    }
                }
            }
        }
        List<MeshFilter[]> result = new List<MeshFilter[]>();
        for (int i = 0; i < splittedByShaderMeshFilters.Length; i++)
        {
            result.Add(splittedByShaderMeshFilters[i].ToArray());
        }
        return result;
    }

}

public struct BakeStruct
{
    public Material[] materials;
    public MeshFilter[] filters;
    public MeshRenderer[] renderers;
}
