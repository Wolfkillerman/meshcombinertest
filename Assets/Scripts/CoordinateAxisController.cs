﻿using UnityEngine;

public class CoordinateAxisController
{
    private Vector3 originalPosition;
    private Quaternion originalRotation;

    private void SaveOriginalPosition(Transform transform)
    {
        originalPosition = transform.position;
    }

    private void SaveOriginalRotation(Transform transform)
    {
        originalRotation = transform.rotation;
    }

    private void MoveToOriginalPosition(Transform transform)
    {
        transform.position = originalPosition;
    }

    private void RotateToOriginalRotation(Transform transform)
    {
        transform.rotation = originalRotation;
    }

    private void MoveToZeroPosition(Transform transform)
    {
        transform.position = Vector3.zero;
    }

    private void RotateToZeroRotation(Transform transform)
    {
        transform.rotation = Quaternion.identity;
    }

    public void PrepareTransformCoordinates(Transform transform)
    {
        SaveOriginalPosition(transform);
        SaveOriginalRotation(transform);
        MoveToZeroPosition(transform);
        RotateToZeroRotation(transform);
    }

    public void ReturnTransformCoordinatesToOriginal(Transform transform)
    {
        MoveToOriginalPosition(transform);
        RotateToOriginalRotation(transform);
    }
}
