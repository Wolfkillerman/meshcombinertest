﻿using System.Collections.Generic;
using UnityEngine;

public class TexturesCreator
{
    private const int MaximalTextureSize = 8192;
    private static Color NormalColor = new Color(0.5f, 0.5f, 1f);

    public static Texture2D CreateTexture(Material[] materials, out Rect[] texRects, out int[] materialIndexes)
    {
        List<List<Texture2D>> texAtlas = new List<List<Texture2D>>();
        List<int> materialIndexesList = new List<int>();
        for (int i = 0; i < materials.Length; i++)
        {
            var tex = materials[i].mainTexture;
            materialIndexesList.Add(i);
            List<Texture2D> matTextures = new List<Texture2D>();
            Texture2D tex2d = new Texture2D(tex.width, tex.height);
            tex2d.SetPixels(((Texture2D)tex).GetPixels());
            matTextures.Add(tex2d);
            //if (materials[i].IsKeywordEnabled("_NORMALMAP"))
            //{
            //    tex = materials[i].GetTexture("_BumpMap");
            //    materialIndexesList.Add(-1);
            //    Texture2D nmap = new Texture2D(tex.width, tex2d.height);
            //    nmap.SetPixels(((Texture2D) tex).GetPixels());
            //    matTextures.Add(nmap);
            //}

            texAtlas.Add(matTextures);
        }

        materialIndexes = materialIndexesList.ToArray();

        int square = 0;
        int texCount = 0;
        foreach (var texList in texAtlas)
        {
            foreach (var tex in texList)
            {
                texCount++;
                square += tex.width * tex.height;
            }
        }


        int atlasSize = Mathf.NextPowerOfTwo((int)Mathf.Pow(square, 0.5f));
        if (atlasSize > MaximalTextureSize)
        {
            texRects = new Rect[0];
            return new Texture2D(0, 0);
        }

        Texture2D atlas = new Texture2D(atlasSize, atlasSize);
        Texture2D[] textures = new Texture2D[texCount];
        texCount = 0;
        foreach (var texList in texAtlas)
        {
            foreach (var tex in texList)
            {
                textures[texCount] = new Texture2D(tex.width, tex.height);
                textures[texCount].SetPixels(tex.GetPixels());
                texCount++;
            }
        }

        texRects = atlas.PackTextures(textures, 0, atlasSize);
        return atlas;
    }

    public static Texture2D CreateTexture(Material[] materials, out Rect[] texRects, out int[] materialIndexes, string currentTexture)
    {
        List<List<Texture2D>> texAtlas = new List<List<Texture2D>>();
        List<int> materialIndexesList = new List<int>();

        bool containAny = false;
        for (int i = 0; i < materials.Length; i++)
        {
            if (materials[i].GetTexture(currentTexture)!= null)
            {
                containAny = true;
                break;
            }
        }

        if (!containAny)
        {
            texRects = new Rect[0];
            materialIndexes = new int [0];
            return new Texture2D(0, 0);
        }

        for (int i = 0; i < materials.Length; i++)
        {
            if (materials[i].GetTexture(currentTexture) != null)
            {
                var tex = materials[i].GetTexture(currentTexture);
                materialIndexesList.Add(i);
                List<Texture2D> matTextures = new List<Texture2D>();
                Texture2D tex2d = new Texture2D(tex.width, tex.height);
                tex2d.SetPixels(((Texture2D)tex).GetPixels());
                matTextures.Add(tex2d);
                texAtlas.Add(matTextures);
            }
            else
            {
                var tex = materials[i].mainTexture;
                materialIndexesList.Add(i);
                List<Texture2D> matTextures = new List<Texture2D>();
                Texture2D tex2d = new Texture2D(tex.width, tex.height);
                Color [] colors = new Color[tex.width * tex.height];
                for (int j = 0; j < tex.width * tex.height; j++)
                {
                    colors[j] = NormalColor;
                }
                tex2d.SetPixels(colors);
                matTextures.Add(tex2d);
                texAtlas.Add(matTextures);
            }
        }

        materialIndexes = materialIndexesList.ToArray();

        int square = 0;
        int texCount = 0;
        foreach (var texList in texAtlas)
        {
            foreach (var tex in texList)
            {
                texCount++;
                square += tex.width * tex.height;
            }
        }

        int atlasSize = Mathf.NextPowerOfTwo((int)Mathf.Pow(square, 0.5f));
        if (atlasSize > MaximalTextureSize)
        {
            texRects = new Rect[0];
            return new Texture2D(0, 0);
        }

        Texture2D atlas = new Texture2D(atlasSize, atlasSize);
        Texture2D[] textures = new Texture2D[texCount];
        texCount = 0;
        foreach (var texList in texAtlas)
        {
            foreach (var tex in texList)
            {
                textures[texCount] = new Texture2D(tex.width, tex.height);
                textures[texCount].SetPixels(tex.GetPixels());
                texCount++;
            }
        }

        texRects = atlas.PackTextures(textures, 0, atlasSize);
        
        return atlas;
    }
}
