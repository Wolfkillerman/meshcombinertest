﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MeshCreator))]
public class MeshCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var myScript = (MeshCreator) target;
        if (GUILayout.Button("Bake"))
        {
            myScript.StartBaking();
        }

        if (GUILayout.Button("Bake with split"))
        {
            myScript.BakeByMaterials();
        }
    }
}
